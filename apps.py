{{ unicode_literals }}from django.apps import AppConfig
from django.conf import settings

from . import constant

class {{ camel_case_app_name }}Config(AppConfig):

    name = '{{ app_name }}'
    verbose_name = constant.APP_VERBOSE_NAME
    
    def ready(self):
        print("App {{ camel_case_app_name }} ready..")
        # import qbase.compat.todjango19
        from . import models, admin, constant
        models.setup_users()
        from django.contrib.auth.admin import UserAdmin
        ###
        if 'qadminMenu' in settings.INSTALLED_APPS: import qadminMenu.plugins
        from django.contrib.admin import site
        site.site_title = settings.SITE_NAME
        site.site_header = settings.SITE_NAME
        ###
        print("App {{ camel_case_app_name }} already")
