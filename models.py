{{ unicode_literals }}from django.db import models, transaction
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model
from django.conf import settings

USER = settings.AUTH_USER_MODEL
def setup_users():
    _globals = globals()
    if 'User' not in _globals:
        _globals['User'] = user_model = get_user_model()
        _globals['Users'] = user_model.objects

# Create your models here.

from . import constant

class Userinfo(models.Model):

    class Meta:
        verbose_name = verbose_name_plural = constant.MODEL_USERINFO

    user = models.OneToOneField(USER, primary_key=True, null=False)

    def __str__(self): return str(self.user)

Userinfos = Userinfo.objects
