from django.http import JsonResponse, HttpResponse
from django.core.urlresolvers import reverse

from .. import models, constant

def build_sitemap(request):
    return {
        'host': request.get_host(),
    }

# @query_with_version_wrapper
def sitemap_json(request):
    return JsonResponse(build_sitemap(request))

from django.template import Context, Engine
import json

js_sitemap_template = r'''
{% templatetag openblock %} autoescape off {% templatetag closeblock %}
(function (globals) {
  globals.sitemap = {% templatetag openvariable %} map_str {% templatetag closevariable %}

}(this));
{% templatetag openblock %} endautoescape {% templatetag closeblock %}
'''
# @query_with_version_wrapper
def sitemap_js(request):
    template = Engine().from_string(js_sitemap_template)
    indent = lambda s: s.replace('\n', '\n  ')
    map = build_sitemap(request)
    context = Context({'map_str': indent(json.dumps(map, sort_keys=True, indent=2))})
    return HttpResponse(template.render(context), 'text/javascript')
