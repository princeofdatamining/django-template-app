from rest_framework import serializers, validators, permissions, generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from .. import models, constant

class ExtraSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Userinfo
        fields = ('user',)

#

class UserSerializer(serializers.ModelSerializer):

    extra = ExtraSerializer(source='userinfo')
    
    class Meta:
        model = models.User
        fields = ('id', 'username', 'extra')

class UserViewMixin(object):

    queryset = models.Users.all()
    serializer_class = UserSerializer

class UsersView(UserViewMixin, generics.ListAPIView):

    pass

class UserView(UserViewMixin, generics.RetrieveAPIView):

    pass

#

from django.conf.urls import url, include
urlpatterns = [
    url(r'^users/$', UsersView.as_view(), name='users'),
    url(r'^users/(?P<pk>\d+)$', UserView.as_view(), name='user-by-pk'),
]
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns)
