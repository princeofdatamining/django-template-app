from django import template
from django.utils.safestring import mark_safe

register = template.Library()

from .. import models, constant
