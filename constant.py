from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_VERBOSE_NAME = pgettext_lazy('App', '{{ camel_case_app_name }}')

MODEL_USERINFO = pgettext_lazy('Model', 'User extra info')
