from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Group
from django.conf import settings
from django.utils.translation import activate, ugettext_lazy as _, pgettext_lazy

from ... import models, constant

def force_user(*args, **kwargs):
    if args:
        username = args[0]
    else:
        username = kwargs.pop('username', None)
    if not username:
        return print("username not set")
    password = kwargs.pop('password', '!{{ camel_case_app_name }}')
    try:
        return models.Users.get(username=username)
    except models.ObjectDoesNotExist:
        pass
    user = models.Users.model(username=username, **kwargs)
    user.set_password(password)
    user.save()
    return user

class Command(BaseCommand):

    help = "Build data"

    def add_arguments(self, parser):
        parser.add_argument('action', nargs='?', help='action ?')

    def handle(self, *args, **options):
        action = options.get('action')
        if not action:
            return
        action = 'build_{}'.format(action)
        if hasattr(self, action):
            return getattr(self, action)()
        action and print("Unknown method:", action)

    def handle(self, *args, **options):
        activate(settings.LANGUAGE_CODE)
        import re
        self._build_pattern = re.compile(r'^build(_+)(\w+)')
        #
        what = options.get('action')
        if not what:
            return
        for action in map(str.strip, what.split(',')):
            if action == 'prod':
                self._build_spec(1, '*')
            elif action == 'dev':
                self._build_spec(2, '*')
            elif action == 'test':
                self._build_spec(3, '*')
            elif not self._build_spec('*', action):
                print("Unknown builder:", action)

    def _build_spec(self, n, spec):
        funcs = []
        for attr in dir(self):
            m = self._build_pattern.match(attr)
            if not m or not all([
                n in ['*', len(m.group(1))],
                spec in ['*', '', None, m.group(2)],
            ]):
                continue
            func = getattr(self, attr)
            if not callable(func):
                continue
            funcs.append(attr)
        # 
        sorted_funcs = sorted(funcs)
        def cmp(e):
            if e in self.system_funcs:
                return self.system_funcs.index(e)
            return len(self.system_funcs) + sorted_funcs.index(e)
        funcs.sort(key=cmp)
        #
        for attr in funcs:
            getattr(self, attr)()
        return bool(funcs)

    system_funcs = (
        'release',
        'menu',
        'user',
    )

    def build___alpha(self):
        print("Build test: alpha")

    def build__beta(self):
        print("Build dev: beta")

    def build_release(self):
        print("Build release")

    def build_menu(self):
        print("Build admin menus")
        '''
        from qadminMenu.models import rebuild
        rebuild([
            ('系统设定', [
                models.User,
                Group,
            ]),
        ])
        # '''
        pass

    def build_user(self):
        print("Build users")
        self.super = force_user('super', is_superuser=True, is_staff=True)
        self.admin = force_user('admin', is_staff=True)
